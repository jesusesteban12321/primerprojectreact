import React from 'react'
import img1 from "../assets/images/shape/shape-1.svg";
import img2 from "../assets/images/shape/shape-2.svg";
import img3 from "../assets/images/shape/shape-3.svg";
import img4 from "../assets/images/shape/shape-4.svg";
import img5 from "../assets/images/shape/shape-1.svg";
import img6 from "../assets/images/shape/shape-4.svg";
import img7 from "../assets/images/shape/shape-3.svg";
import img8 from "../assets/images/shape/shape-2.svg";
import img9 from "../assets/images/shape/shape-4.svg";
import img10 from "../assets/images/shape/shape-1.svg";
import img11 from "../assets/images/shape/shape-2.svg";
import img12 from "../assets/images/shape/shape-2.svg";
import imgHeader from "../assets/images/header-image.svg";

function Header() {
  return (
    <div>
      <div id="home" className="header_hero bg-gray relative z-10 overflow-hidden lg:flex items-center">
            <div className="hero_shape shape_1">
                <img src={img1} alt="shape"/>
            </div>
            <div className="hero_shape shape_2">
                <img src={img2} alt="shape"/>
            </div>
            <div className="hero_shape shape_3">
                <img src={img3} alt="shape"/>
            </div>
            <div className="hero_shape shape_4">
                <img src={img4} alt="shape"/>
            </div>
            <div className="hero_shape shape_6">
                <img src={img5} alt="shape"/>
            </div>
            <div className="hero_shape shape_7">
                <img src={img7} alt="shape"/>
            </div>
            <div className="hero_shape shape_8">
                <img src={img8} alt="shape"/>
            </div>
            <div className="hero_shape shape_9">
                <img src={img9} alt="shape"/>
            </div>
            <div className="hero_shape shape_10">
                <img src={img10} alt="shape"/>
            </div>
            <div className="hero_shape shape_11">
                <img src={img11} alt="shape"/>
            </div>
            <div className="hero_shape shape_12">
                <img src={img12} alt="shape"/>
            </div>

            <div className="container">
                <div className="row">
                    <div className="w-full lg:w-1/2">
                        <div className="header_hero_content pt-150 lg:pt-0">
                            <h2 className="hero_title text-2xl sm:text-4xl md:text-5xl lg:text-4xl xl:text-5xl font-extrabold">Creative Multipurpose Tailwind CSS <span className="text-theme-color">Template</span></h2>
                            <p className="mt-8 lg:mr-8">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna.</p>
                            <div className="hero_btn mt-10">
                                <a className="main-btn" href="#0">Get Started</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className="header_shape hidden lg:block"></div>

            <div className="header_image flex items-center">
                <div className="image 2xl:pl-25">
                    <img src={imgHeader } alt="Header Image"/>
                </div>
            </div>
        </div>
    </div>
  )
}

export default Header

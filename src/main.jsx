import React from 'react'
import ReactDOM from 'react-dom/client'
import './index.css'
import App from './App'
import Home from './routes/Home'
import Curriculum from './routes/Curriculum'
import NotFounds from './routes/NotFounds'
import { createBrowserRouter, RouterProvider } from "react-router-dom"
 
const router = createBrowserRouter([
  {
    path: "/",
    element: <Home/>,
  },
  {
    path: "/Curriculum",
    element: <Curriculum/>,
  },
  {
    path: "*",
    element: <NotFounds/>,
  },
]);

ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
    <RouterProvider router={router} />
  </React.StrictMode>
)

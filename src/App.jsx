import { useEffect, useState } from 'react'
import { Routes } from 'react-router-dom'
import Curriculum from './routes/Curriculum'
import Home from './routes/Home'
import NotFounds from './routes/NotFounds'


function App() {
  
  return (
    <div>
      <Routes>
          <Route path="/" element={ <Home/> } />
          <Route path="Curriculum" element={ <Curriculum/> } />
          <Route path="*" element={ <NotFounds/> } />
      </Routes>
    </div>
  )
}

export default App

import React, { useEffect, useState } from 'react'
import Footer from '../componets/Footer'
import Header2 from '../componets/Header2'
import Product from '../componets/Product'
import Sep from '../componets/Sep'
import Blog from '../componets/Blog'
import Side from '../componets/Side'
import Price from '../componets/Price'
import Description from '../componets/Description'

function Home() {
    const uri = "https://rickandmortyapi.com/api/"
    const character = "character"

    const [queryCharacter, setQueryCharacter] = useState()
    const [count, setCount] = useState(0)
    const fetchApi = async () => {
        const response = await fetch(uri + character )
        const responseJSON = await response.json()
        setQueryCharacter(responseJSON)
    }
    useEffect(()=>{
        fetchApi()
    },[])

  return (
    <div>
        <Header2/>
        <Product queryCharacter={queryCharacter}/>
        <Side/>
        <Blog/>
        <Description/>
        <Price/>
        <Sep/>
        <Footer/>
    </div>
  )
}

export default Home

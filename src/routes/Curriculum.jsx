import React from 'react'
import Otho from '../componets/Otho'
import Footer from '../componets/Footer'
import Header2 from '../componets/Header2'
import Content from '../componets/Content'
import Logos from '../componets/Logos'
import Sep from '../componets/Sep'
import Dr from '../componets/Dr'
import Side from '../componets/Side'


function Curriculum() {
  
  return (
    <div>
        <Header2/>
        <Otho></Otho>
        <Content/>
        <Side/>
        <Dr/>
        <Sep/>
        <Logos/>
        <Footer/>
    </div>
  )
}

export default Curriculum
